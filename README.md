Ansible role - Install CA
=========

Very simple role that installs a certificate from another server into 
the OS's trusted certificates collection. Only tested in Ubuntu 2004.

Requirements
------------

None.

Role Variables
--------------

Very simple role that fetchs a certificate from a remote system (named as
an ansible inventory host, accesible thorugh ansible directoy) and
installs it in the local trusted certificate collections. The role is
developed with Ubuntu 20.04 in mind and meant to be used in conjunction
with other roles developed in 
[this gitlab group](https://gitlab.com/galvesband-ansible).

Dependencies
------------

None.

Example Playbook
----------------

```yml
---

- hosts: my-server
  gather_facts: yes
  roles:
  # Builds a simple CA certificate in /srv/ca/my-example.ca/ca.{crt,key}
  - name: Create a simple CA
    include_role:
      name: certificate_authority
    vars:
      ca_name: my-example.ca

  # Installs ca.crt from my-server:/srv/ca/my-example.ca/ca.crt into the system.
  - name: Installs ca certificate in OS
    include_role:
      name: install_ca
    vars:
      install_ca_certificates:
        - ca_name: my-example.ca
          path: /srv/ca/my-example.ca/ca.crt
          host: my-server
```

License
-------

[GPL 2.0 Or later](https://spdx.org/licenses/GPL-2.0-or-later.html).
